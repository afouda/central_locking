
import datetime
import threading
import thread 
from datetime import timedelta
import time
import random
import Queue



waiting_dict = {}
locked_resources = {}
service_ttl = {}

def acquire_access(service_id, resource):
    available = is_available(resource)
    if available:
        #TODO lock this resource
        locked_resources[resource] = service_id
        service_ttl[service_id] = datetime.datetime.now()
        state = "locked"
        status_code = 200
    else:
        waiting_dict[service_id] = {"time" : datetime.datetime.now(), "resource": resource}
        available = check_time_out(resource, service_id)
        if available:
            locked_resources[resource] = service_id
            service_ttl[service_id] = datetime.datetime.now()
            state = "locked"
            status_code = 200
        else:
            state = "request timeout "
            status_code = 408
    return {"resource":resource, "state":state, "status_code":status_code}

def release_access(service_id, resource):
    if locked_resources.get(resource) and \
    locked_resources.get(resource) == service_id:
        del locked_resources[resource]
        state = "released"
        response = "200"
    else:
        state = "can not release this resource"
        response = "403"

    return {"resource": resource, "state":state, "status_code":response}


def is_available(resource):
    if locked_resources.get(resource):
        return False
    return True

def check_time_out(resource, service_id):
    available = True
    while not is_available(resource):
        now = datetime.datetime.now()
        #TODO timeout will be from conf file 
        if (now - waiting_dict[service_id]["time"]).seconds > 15:
            del waiting_dict[service_id]
            #we will get this from conf file later
            available = False
            break
        #we will get this from conf file later    
        time.sleep(5)
    return available


def check_deadlock():
    pass

def detect_service_crashing():
    while True:
        now = datetime.datetime.now()
        for key in service_ttl:
            if (now - service_ttl[key]).seconds > 15:
                for resource in locked_resources:
                    if locked_resources[resource] == key:
                        del locked_resources[resource]
                        del service_ttl[service_id]
        time.sleep(5)

def update_service_ttl(service_id):
    service_ttl[service_id] = datetime.datetime.now()
    return {'status_code':200}

