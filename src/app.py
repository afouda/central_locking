from flask import Flask, jsonify, request, abort
# from central_locking_manager import *
import locking_manager as locker
import thread 

app = Flask(__name__)
thread.start_new_thread(locker.detect_service_crashing, ())

@app.route('/resources/<resource>/lock', methods=['GET'])
def acquire_access(resource):
    args = request.args
    response = locker.acquire_access(args["service_id"], resource)
    return jsonify({'response': response}), response['status_code']

@app.route('/resources/<resource>/release', methods=['GET'])
def release_access(resource):
    args = request.args
    response = locker.release_access(args["service_id"], resource)
    return jsonify({'response': response}), response['status_code']

@app.route('/service/<service_id>', methods=['PUT'])
def update_service_ttl(service_id):
    locker.update_service_ttl(service_id)
    return jsonify({'response': response}), response['status_code']

if __name__ == '__main__':
    app.run(debug=True, threaded=True)