# README #

### What is this repository for? ###

* Central Locking System

### How do I get set up and running? ###

* Install requirements from requirements.txt (pip install -r requirements.txt)
* Run by (python src/app.py)
* Test by (nosetests)