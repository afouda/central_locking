import unittest
from src import locking_manager

class LockingManagerTest(unittest.TestCase):
    def test_acquire_access(self):
        response = locking_manager.acquire_access("service_1", "resource_1")
        self.assertEqual(response["state"] , "locked")

    def test_release_access(self):
        locking_manager.acquire_access("service_2", "resource_2")
        response = locking_manager.release_access("service_2", "resource_2")
        self.assertEqual(response["state"], "released")



